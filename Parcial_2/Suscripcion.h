#include<String>

using namespace std;

class Suscripcion{
	
	public:
		virtual void diasRestantes()=0;
		virtual void fechaInicio()=0;
		virtual void fechaFin()=0;
	
};

